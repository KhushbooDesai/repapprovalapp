package com.example.approvalapp.Interface;


import com.example.approvalapp.Model.LoginResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface Api
{

    //@FormUrlEncoded
    //login is the  controller method
    @GET("Login")
    //PSNO is table field
    Call<LoginResponse> loginCheck(@Query("Psno") String psno, @Query("Password") String Password);
}
