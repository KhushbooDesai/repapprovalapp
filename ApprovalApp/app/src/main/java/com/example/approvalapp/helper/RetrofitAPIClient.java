package com.example.approvalapp.helper;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitAPIClient
{
    private static Retrofit retrofit = null;


    public static Retrofit getClient()
    {
        retrofit = new Retrofit.Builder()
                .baseUrl("http://10.7.66.32:8085/approvalappservices/api/Approval/")
                //.baseUrl("http://phzpdshpdev:8085/smartchatservices/Home/")
                //.baseUrl("https://ecom.lthed.com/portal/PowerBiServices/home/")
                //.baseUrl("http://10.7.66.32:8085/ApprovalAppServices/Home/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit;
    }
}
