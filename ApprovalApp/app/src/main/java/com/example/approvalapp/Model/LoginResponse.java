package com.example.approvalapp.Model;

import com.google.gson.annotations.SerializedName;

public class LoginResponse
{
    @SerializedName("loginStatus")
    private String loginStatus;

    @SerializedName("Name")
    private String Name;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getLoginStatus() {
        return loginStatus;
    }

    public void setLoginStatus(String loginStatus) {
        this.loginStatus = loginStatus;
    }
}
